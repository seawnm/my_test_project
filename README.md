# 202002_kaggle

## 練習:
### 模仿他人的kaggle code

   請Copy W1指定的Notebook到自己的kaggle notebook上[Feature engineering, xgboost](https://www.kaggle.com/dlarionov/feature-engineering-xgboost)

## 作業

>請試著修改作者的Notebook，看底下的兩個情境XGBoost的分數變高or低？

### 1. 目前模型最重要的變數，將多餘的期別刪除

```python
ts = time.time()
group = matrix.groupby(['date_block_num', 'item_id']).agg({'item_cnt_month': ['mean']})
group.columns = [ 'date_item_avg_item_cnt' ]
group.reset_index(inplace=True)

matrix = pd.merge(matrix, group, on=['date_block_num','item_id'], how='left')
matrix['date_item_avg_item_cnt'] = matrix['date_item_avg_item_cnt'].astype(np.float16)
# 請將下列2,3,6,12數字刪除，看XGBoost訓練完的分數變高或低？
matrix = lag_feature(matrix, [1,2,3,6,12], 'date_item_avg_item_cnt')
matrix.drop(['date_item_avg_item_cnt'], axis=1, inplace=True)
time.time() - ts

```

#### XGBoost分數比較:

- 原本:
  
   Stopping. Best iteration:
   [45]	validation_0-rmse:0.813137	validation_1-rmse:0.905782

- 調整後:

   Stopping. Best iteration:
   [56]	validation_0-rmse:0.808488	validation_1-rmse:0.908775


#### Feature Important比較:
- 原本:

![](pic/orig_fi.png)

- 調整後:

![](pic/h1_fi.png)



### 2. 目前模型第二重要的變數，增加多餘的期別

```python
ts = time.time()
group = matrix.groupby(['date_block_num', 'shop_id', 'item_category_id']).agg({'item_cnt_month': ['mean']})
group.columns = ['date_shop_cat_avg_item_cnt']
group.reset_index(inplace=True)

matrix = pd.merge(matrix, group, on=['date_block_num', 'shop_id', 'item_category_id'], how='left')
matrix['date_shop_cat_avg_item_cnt'] = matrix['date_shop_cat_avg_item_cnt'].astype(np.float16)
# 將1後面加上3,6,9，看XGBoost的分數變高或變低？
matrix = lag_feature(matrix, [1], 'date_shop_cat_avg_item_cnt')
matrix.drop(['date_shop_cat_avg_item_cnt'], axis=1, inplace=True)
time.time() - ts
```

#### XGBoost分數比較:

- 原本:
  
   Stopping. Best iteration:
   [45]	validation_0-rmse:0.813137	validation_1-rmse:0.905782

- 調整後:

   Todo


#### Feature Important比較:
- 原本:

![](pic/orig_fi.png)

- 調整後:

![]()


### 最佳Score

![]()


### 程式碼
[Feature engineering, xgboost.ipynb](Feature%20engineering,%20xgboost.ipynb)

